@echo off
set VERSION=%1
set FILE=S3PointBrowser-%VERSION%.jar

gradlew build -Pversion="%VERSION%" && copy "build\libs\%FILE%" "S3PointBrowser.jar"