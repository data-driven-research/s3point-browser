#!/bin/bash

VERSION=$1
FILE=S3PointBrowser-$VERSION.jar

./gradlew build -Pversion="$VERSION"
cp "build/libs/$FILE" "S3PointBrowser.jar"
