plugins {
    java
    id("org.springframework.boot") version "3.3.0"
    id("io.spring.dependency-management") version "1.1.5"
}

group = "cz.tul.cxi"
version = project.version
java.sourceCompatibility = JavaVersion.VERSION_21

repositories {
    mavenCentral()
    maven { setUrl("https://build.shibboleth.net/nexus/content/repositories/releases/") }
}

dependencies {
    constraints {
        implementation("org.opensaml:opensaml-core:4.3.2")
        implementation("org.opensaml:opensaml-saml-api:4.3.2")
        implementation("org.opensaml:opensaml-saml-impl:4.3.2")
        implementation("ch.qos.logback:logback-classic:1.4.14")

    }
    implementation(platform("software.amazon.awssdk:bom:2.25.60"))
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.hibernate.validator:hibernate-validator")
    implementation("org.springframework.session:spring-session-core")
    implementation("org.springframework.security:spring-security-saml2-service-provider")
    implementation("org.thymeleaf.extras:thymeleaf-extras-springsecurity6")
    implementation("com.fasterxml.jackson.module:jackson-module-jakarta-xmlbind-annotations")
    implementation("jakarta.json:jakarta.json-api:2.1.3")
    implementation("commons-io:commons-io:2.16.1")
    implementation("software.amazon.awssdk:s3")
    implementation("org.liquibase:liquibase-core")
    compileOnly("org.projectlombok:lombok")
    runtimeOnly("org.postgresql:postgresql")
    annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")
    annotationProcessor("org.projectlombok:lombok")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.springframework.security:spring-security-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.10.2")
    testImplementation("org.testcontainers:postgresql:1.19.8")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}


tasks.test {
    // onlyIf { project.version != "production" }
    onlyIf { project.version == "none" }
    useJUnitPlatform()
}
