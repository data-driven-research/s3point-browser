package cz.tul.cxi.s3point.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tul.cxi.s3point.component.S3CoreComponent;
import cz.tul.cxi.s3point.model.jpa.User;
import cz.tul.cxi.s3point.model.s3.S3Bucket;
import cz.tul.cxi.s3point.model.s3.S3Object;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class S3CoreService {

    private final S3CoreComponent s3CoreComponent;

    private final ObjectMapper mapper;

    public S3CoreService(S3CoreComponent s3CoreComponent,
                         ObjectMapper mapper) {
        this.s3CoreComponent = s3CoreComponent;
        this.mapper = mapper;
    }

    public String listBuckets() {
        log.debug("Listing buckets");
        String buckets = s3CoreComponent.listBuckets();
        log.debug("Buckets: {}", buckets);

        return buckets;
    }

    public String listDatasets(S3Bucket s3Bucket, User user) {
        log.debug("Listing datasets");
        String datasets;
        try {
            datasets = s3CoreComponent.listDatasets(s3Bucket, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        log.debug("Datasets: {}", datasets);

        return datasets;
    }

    public String listFiles(S3Object s3Dataset, User user) {
        log.debug("Listing files");
        String files = null;
        try {
            files = s3CoreComponent.listObjects(s3Dataset, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        log.debug("Files: {}", files);

        return files;
    }

    public String downloadS3Dataset(S3Object s3Dataset, User user) {
        log.debug("Downloading dataset");
        String urls = null;
        try {
            urls = s3CoreComponent.getPreSignedULRs(s3Dataset, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        log.debug("Dataset files: {}", urls);

        return urls;
    }

    public String downloadFile(S3Object s3Object, User user) {
        log.debug("Downloading file");
        String urls = null;
        try {
            urls = s3CoreComponent.downloadFile(s3Object, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        log.debug("File: {}", urls);

        return urls;
    }

    public void addUserToDataset(S3Object dataset, User user) {
        try {
            s3CoreComponent.shareDataset(dataset, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    public void createBucket(S3Bucket s3Bucket) {
        log.debug("Creating bucket");
        try {
            s3CoreComponent.createBucket(s3Bucket);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void createDataset(S3Object dataset, User user) {
        log.debug("Creating dataset");
        try {
            s3CoreComponent.createDataset(dataset, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPublicURLs(S3Object s3Dataset, User user) {
        log.debug("Sharing public URLs");
        String urls = null;
        try {
            urls = s3CoreComponent.getPreSignedULRs(s3Dataset, user.getEinfraId());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        log.debug("URLs: {}", urls);

        return urls;
    }

    public void removeUserFromDataset(S3Object dataset, User user) {
        s3CoreComponent.removeUserFromDataset(dataset, user.getEinfraId());
    }

    public boolean userIsBucketManager(S3Object dataset, User user) {
        return s3CoreComponent.userIsBucketManager(new S3Bucket(dataset.getBucketName()), user.getEinfraId());
    }

    public String findUsersOfS3Dataset(S3Object s3dataset) {
        return s3CoreComponent.findUsersOfS3Dataset(s3dataset);
    }
}
