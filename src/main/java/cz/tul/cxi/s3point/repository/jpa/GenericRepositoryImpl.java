package cz.tul.cxi.s3point.repository.jpa;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.PersistenceContext;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.fasterxml.jackson.databind.ObjectMapper;

import cz.tul.cxi.s3point.model.jpa.User;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonValue;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.JoinType;
import jakarta.persistence.criteria.Order;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings("unchecked")
public class GenericRepositoryImpl<T, ID extends Serializable>
        extends SimpleJpaRepository<T, ID> implements GenericRepository<T, ID> {

    private final Class<T> type;

    private final ObjectMapper mapper;

    @PersistenceContext
    private final EntityManager em;

    public GenericRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        this.type = (Class<T>) entityInformation.getJavaType();
        log.debug("Type: " + this.type.getName());
        log.debug("Class: " + this.getClass().getName());
        this.mapper = new ObjectMapper();
        this.em = entityManager;
    }

    @Override
    public final String getPQGridData(String userEmail,
                                      int page,
                                      int limit,
                                      String pq_sort,
                                      String pq_filter,
                                      String pq_filter2,
                                      int joinTable) throws Exception {

        final CriteriaBuilder cb = em.getCriteriaBuilder();

        // setup initial params from pqGrid
        if (page == 0)
            page = 1;

        JsonObject[] filters = new JsonObject[2];
        if (pq_filter != null)
            filters[0] = Json.createReader(new StringReader(pq_filter)).readObject();
        if (pq_filter2 != null)
            filters[1] = Json.createReader(new StringReader(pq_filter2)).readObject();

        JsonArray sort = null;
        if (pq_sort != null)
            sort = Json.createReader(new StringReader(pq_sort)).readArray();

        log.debug("1");

        // total query
        final CriteriaQuery<Long> criteria1 = cb.createQuery(Long.class);
        Root<T> root1 = criteria1.from(this.type);
        if (joinTable == JT_USER_ASSOCIATION) {
            Join<User, T> bJoin = root1.join("users", JoinType.INNER);
            bJoin.on(cb.equal(bJoin.get("email"), userEmail));
        }
        criteria1.select(cb.count(root1));

        log.debug("2");

        // filters
        List<Predicate> predicates1 = new ArrayList<>();
        for (JsonObject filter : filters) {
            List<Predicate> whereList = new ArrayList<>();
            if (filter != null) {
                for (JsonValue filterItem : filter.getJsonArray("data")) {
                    JsonObject item = filterItem.asJsonObject();
                    if (item.getString("condition").equalsIgnoreCase("equal"))
                        whereList.add(cb.equal(root1.get(item.getString("dataIndx")).as(String.class),
                                item.getString("value")));
                    if (item.getString("condition").equalsIgnoreCase("notequal"))
                        whereList.add(cb.notEqual(root1.get(item.getString("dataIndx")).as(String.class),
                                item.getString("value")));
                    if (item.getString("condition").equalsIgnoreCase("contain"))
                        whereList.add(cb.like(root1.get(item.getString("dataIndx")).as(String.class),
                                "%" + item.getString("value") + "%"));
                }
                predicates1.add((filter.getString("mode").equals("OR")) ? cb.or(whereList.toArray(new Predicate[0]))
                        : cb.and(whereList.toArray(new Predicate[0])));
            }
        }

        Predicate finalCondition1 = cb.and(predicates1.toArray(new Predicate[0]));

        criteria1.where(finalCondition1);

        Query query1 = em.createQuery(criteria1);

        String sql = query1.unwrap(org.hibernate.query.Query.class).getQueryString();
        log.debug(sql);

        // run query
        Long totalRecords = (Long) query1.getSingleResult();

        log.debug("4");

        // data query
        final CriteriaQuery<T> criteria2 = cb.createQuery(this.type);
        Root<T> root2 = criteria2.from(this.type);
        if (joinTable == JT_USER_ASSOCIATION) {
            Join<User, T> bJoin = root2.join("users", JoinType.INNER);
            bJoin.on(cb.equal(bJoin.get("email"), userEmail));
        }
        criteria2.select(root2);

        // filters
        List<Predicate> predicates2 = new ArrayList<>();
        for (JsonObject filter : filters) {
            List<Predicate> whereList = new ArrayList<>();
            if (filter != null) {
                for (JsonValue filterItem : filter.getJsonArray("data")) {
                    JsonObject item = filterItem.asJsonObject();
                    if (item.getString("condition").equalsIgnoreCase("equal"))
                        whereList.add(cb.equal(root2.get(item.getString("dataIndx")).as(String.class),
                                item.getString("value")));
                    if (item.getString("condition").equalsIgnoreCase("notequal"))
                        whereList.add(cb.notEqual(root2.get(item.getString("dataIndx")).as(String.class),
                                item.getString("value")));
                    if (item.getString("condition").equalsIgnoreCase("contain"))
                        whereList.add(cb.like(root2.get(item.getString("dataIndx")).as(String.class),
                                "%" + item.getString("value") + "%"));
                }
                predicates2.add((filter.getString("mode").equals("OR")) ? cb.or(whereList.toArray(new Predicate[0]))
                        : cb.and(whereList.toArray(new Predicate[0])));
            }
        }

        Predicate finalCondition2 = cb.and(predicates2.toArray(new Predicate[0]));

        criteria2.where(finalCondition2);

        // sort
        if (sort != null) {
            List<Order> orderList = new ArrayList<>();
            for (JsonValue sortItem : sort) {
                JsonObject item = sortItem.asJsonObject();
                orderList.add(("down".equals(item.getString("dir")) ? cb.desc(root2.get(item.getString("dataIndx")))
                        : cb.asc(root2.get(item.getString("dataIndx")))));
            }
            criteria2.orderBy(orderList);
        }

        final Query query2 = em.createQuery(criteria2);
        query2.setFirstResult((page - 1) * limit);
        query2.setMaxResults(limit);
        List<T  > list = query2.getResultList();

        log.debug("6");

        JsonObjectBuilder job = Json.createObjectBuilder();
        JsonArrayBuilder jab = Json.createArrayBuilder();

        for (T entity : list) {
            log.debug(mapper.writeValueAsString(entity));
            JsonObject jo = Json.createReader(new StringReader(mapper.writeValueAsString(entity))).readObject();
            jab.add(jo);
        }
        JsonArray ja = jab.build();

        job.add("curPage", page);
        job.add("totalRecords", totalRecords);
        job.add("data", ja);

        JsonObject result = job.build();

        log.debug(result.toString());
        log.debug("7");

        return result.toString();
    }

}
