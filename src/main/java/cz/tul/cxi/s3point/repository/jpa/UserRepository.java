package cz.tul.cxi.s3point.repository.jpa;

import cz.tul.cxi.s3point.model.jpa.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User, String> {
}
