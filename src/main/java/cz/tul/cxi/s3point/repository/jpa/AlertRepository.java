package cz.tul.cxi.s3point.repository.jpa;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cz.tul.cxi.s3point.model.jpa.Alert;

@Repository
public interface AlertRepository extends GenericRepository<Alert, Long> {
    @Query("SELECT COUNT(a) FROM Alert a JOIN User u ON u.email = a.user.email WHERE a.user.email = :userId")
    Long countByUserId(String userId);
}
