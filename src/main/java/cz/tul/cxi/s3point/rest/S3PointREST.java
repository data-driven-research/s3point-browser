package cz.tul.cxi.s3point.rest;

import cz.tul.cxi.s3point.model.s3.S3Bucket;
import cz.tul.cxi.s3point.model.s3.S3Object;
import cz.tul.cxi.s3point.service.S3CoreService;
import cz.tul.cxi.s3point.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/s3point")
public class S3PointREST {

    private final S3CoreService s3CoreService;

    private final UserService userService;

    public S3PointREST(S3CoreService s3CoreService,
                       UserService userService) {
        this.s3CoreService = s3CoreService;
        this.userService = userService;
    }

    @PostMapping(value = "/pqgrid/bucket", produces = {"application/json"})
    public ResponseEntity<Object> pqGridBucket(Authentication authentication) {
        try {
            final String result = s3CoreService.listBuckets();
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/pqgrid/dataset", produces = {"application/json"})
    public ResponseEntity<Object> pqGridDataset(Authentication authentication,
                                                @RequestParam("name") String bucketName) {
        try {
            final String result = s3CoreService.listDatasets(new S3Bucket(bucketName),
                    userService.findById(authentication.getName()).orElseThrow());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(value = "/pqgrid/file", produces = {"application/json"})
    public ResponseEntity<Object> pqGridFile(Authentication authentication,
                                             @RequestParam("bucketName") String bucketName,
                                             @RequestParam("prefix") String prefix,
                                             @RequestParam("name") String name) {
        try {
            final String result = s3CoreService.listFiles(new S3Object(bucketName, name, prefix),
                    userService.findById(authentication.getName()).orElseThrow());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
