package cz.tul.cxi.s3point.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import cz.tul.cxi.s3point.utility.ListOfStringsToStringConverter;
import cz.tul.cxi.s3point.utility.StringArrayToStringConverter;
import cz.tul.cxi.s3point.utility.StringToDurationConverter;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToDurationConverter());
        registry.addConverter(new ListOfStringsToStringConverter());
        registry.addConverter(new StringArrayToStringConverter());
    }

    @Bean
    @RequestScope
    public ServletUriComponentsBuilder urlBuilder() {
        return ServletUriComponentsBuilder.fromCurrentRequest();
    }

}
