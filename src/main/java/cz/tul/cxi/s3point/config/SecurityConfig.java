/*
 * Copyright 2002-2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cz.tul.cxi.s3point.config;

import cz.tul.cxi.s3point.model.auxiliary.UserRole;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.web.DefaultRelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.RelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.Saml2AuthenticationTokenConverter;
import org.springframework.security.saml2.provider.service.web.Saml2MetadataFilter;
import org.springframework.security.saml2.provider.service.web.authentication.Saml2WebSsoAuthenticationFilter;
import org.springframework.security.saml2.provider.service.web.authentication.logout.Saml2LogoutRequestResolver;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@Profile(value = {"production", "stage"})
public class SecurityConfig {

    @Bean
    SecurityFilterChain defaultLogin(HttpSecurity http,
                                     Saml2MetadataFilter filter,
                                     Saml2LogoutRequestResolver logoutRequestResolver,
                                     Saml2LoginConfig config) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests((authorize) -> authorize
                        .requestMatchers("/browserconfig.xml", "/site.webmanifest").permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/css/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/fonts/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/icons/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/js/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/libs/**")).permitAll()
                        .requestMatchers(new AntPathRequestMatcher("/metadata/**")).permitAll()
                        .requestMatchers("/admin*").hasRole(UserRole.ADMINISTRATOR.name())
                        .requestMatchers("/", "/login", "/error", "/logout").permitAll()
                        .anyRequest().authenticated()
                )
                .authenticationProvider(config.createAuthenticationProvider())
                .saml2Login(config)
                .addFilterBefore(filter, Saml2WebSsoAuthenticationFilter.class)
                .saml2Logout((saml2) -> saml2
                        .logoutRequest((request) -> request
                                .logoutRequestResolver(logoutRequestResolver)
                        ))
                .formLogin(form -> form
                        .loginPage("/login")
                )
                .logout(Customizer.withDefaults());
        return http.build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }

    @Bean
    RelyingPartyRegistrationResolver relyingPartyRegistrationResolver(
            RelyingPartyRegistrationRepository registration) {
        return new DefaultRelyingPartyRegistrationResolver((id) -> registration.findByRegistrationId("s3point"));
    }

    @Bean
    Saml2AuthenticationTokenConverter authentication(
            RelyingPartyRegistrationResolver registration) {
        return new Saml2AuthenticationTokenConverter(registration);
    }
}
