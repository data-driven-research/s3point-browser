package cz.tul.cxi.s3point.config;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.opensaml.saml.ext.saml2mdreqinit.RequestInitiator;
import org.opensaml.saml.ext.saml2mdreqinit.impl.RequestInitiatorBuilder;
import org.opensaml.saml.ext.saml2mdui.Description;
import org.opensaml.saml.ext.saml2mdui.DisplayName;
import org.opensaml.saml.ext.saml2mdui.InformationURL;
import org.opensaml.saml.ext.saml2mdui.UIInfo;
import org.opensaml.saml.ext.saml2mdui.impl.DescriptionBuilder;
import org.opensaml.saml.ext.saml2mdui.impl.DisplayNameBuilder;
import org.opensaml.saml.ext.saml2mdui.impl.InformationURLBuilder;
import org.opensaml.saml.ext.saml2mdui.impl.UIInfoBuilder;
import org.opensaml.saml.saml2.core.*;
import org.opensaml.saml.saml2.core.impl.AudienceBuilder;
import org.opensaml.saml.saml2.core.impl.AudienceRestrictionBuilder;
import org.opensaml.saml.saml2.core.impl.ConditionsBuilder;
import org.opensaml.saml.saml2.metadata.Extensions;
import org.opensaml.saml.saml2.metadata.*;
import org.opensaml.saml.saml2.metadata.impl.*;
import org.opensaml.security.credential.UsageType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.security.saml2.core.Saml2X509Credential;
import org.springframework.security.saml2.provider.service.metadata.OpenSamlMetadataResolver;
import org.springframework.security.saml2.provider.service.registration.InMemoryRelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistration;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrationRepository;
import org.springframework.security.saml2.provider.service.registration.RelyingPartyRegistrations;
import org.springframework.security.saml2.provider.service.web.DefaultRelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.RelyingPartyRegistrationResolver;
import org.springframework.security.saml2.provider.service.web.Saml2MetadataFilter;
import org.springframework.security.saml2.provider.service.web.authentication.OpenSaml4AuthenticationRequestResolver;
import org.springframework.security.saml2.provider.service.web.authentication.Saml2AuthenticationRequestResolver;
import org.springframework.security.saml2.provider.service.web.authentication.logout.OpenSaml4LogoutRequestResolver;
import org.springframework.security.saml2.provider.service.web.authentication.logout.Saml2LogoutRequestResolver;

import java.io.InputStream;
import java.security.Security;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;

@Configuration
@Profile(value = {"production", "stage"})
public class Saml2MetadataConfig {

    @Value("${idp.metadata_url}")
    private String IdPMetadataURL;

    @Value("${idp.certificate_file}")
    private String IdPCertificate;

    @Value("${sp.private_key_file}")
    private RSAPrivateKey privateKey;

    @Value("${sp.certificate_file}")
    private String certificate;

    @Value("${sp.base_url}")
    private String baseUrl;

    @Value("${sp.organization.name.en}")
    private String organizationNameENVal;

    @Value("${sp.organization.name.cs}")
    private String organizationNameCZVal;

    @Value("${sp.organization.url.en}")
    private String organizationURLENVal;

    @Value("${sp.organization.url.cs}")
    private String organizationURLCZVal;

    @Value("${sp.contactPerson.givenname}")
    private String contactPersonGivenName;

    @Value("${sp.contactPerson.surname}")
    private String contactPersonSurname;

    @Value("${sp.contactPerson.email}")
    private String contactPersonEmail;


    @Bean
    Saml2AuthenticationRequestResolver authenticationRequestResolver(
            RelyingPartyRegistrationRepository registrations) {
        RelyingPartyRegistrationResolver registrationResolver =
                new DefaultRelyingPartyRegistrationResolver(registrations);
        OpenSaml4AuthenticationRequestResolver authenticationRequestResolver =
                new OpenSaml4AuthenticationRequestResolver(registrationResolver);
        authenticationRequestResolver.setAuthnRequestCustomizer((context) -> {
            context.getAuthnRequest().setForceAuthn(false);
            context.getAuthnRequest().setConditions(conditions());
        });

        return authenticationRequestResolver;
    }

    @Bean
    Saml2LogoutRequestResolver logoutRequestResolver(
            RelyingPartyRegistrationResolver registrationResolver) {
        OpenSaml4LogoutRequestResolver logoutRequestResolver =
                new OpenSaml4LogoutRequestResolver(registrationResolver);
        logoutRequestResolver.setParametersConsumer((parameters) -> {
            String format = "urn:oasis:names:tc:SAML:2.0:nameid-format:transient";
            LogoutRequest logoutRequest = parameters.getLogoutRequest();
            NameID nameId = logoutRequest.getNameID();
            nameId.setNameQualifier(IdPMetadataURL);
            nameId.setSPNameQualifier(baseUrl + "/saml2/service-provider-metadata/s3point");
            nameId.setFormat(format);
        });
        return logoutRequestResolver;
    }

    @Bean
    Saml2MetadataFilter metadata(
            RelyingPartyRegistrationResolver registration) {
        OpenSamlMetadataResolver openSamlMetadataResolver = new OpenSamlMetadataResolver();
        openSamlMetadataResolver.setEntityDescriptorCustomizer(param -> {
            param.getEntityDescriptor().setOrganization(organization());
            param.getEntityDescriptor().getContactPersons().add(contactPerson());
            param.getEntityDescriptor().getSPSSODescriptor("urn:oasis:names:tc:SAML:2.0:protocol")
                    .getKeyDescriptors().get(0).setUse(UsageType.UNSPECIFIED);
            param.getEntityDescriptor().getSPSSODescriptor("urn:oasis:names:tc:SAML:2.0:protocol")
                    .getKeyDescriptors().remove(1);
            param.getEntityDescriptor().getSPSSODescriptor("urn:oasis:names:tc:SAML:2.0:protocol")
                    .setExtensions(extensions());
        });
        return new Saml2MetadataFilter(registration, openSamlMetadataResolver);
    }

    @Bean
    FilterRegistrationBean<Saml2MetadataFilter> filter(
            Saml2MetadataFilter metadata) {
        FilterRegistrationBean<Saml2MetadataFilter> filter = new FilterRegistrationBean<>(metadata);
        filter.setOrder(-101);
        return filter;
    }

    @Bean
    RelyingPartyRegistrationRepository repository() {
        if (Security.getProvider("BouncyCastleProvider") == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
        Saml2X509Credential signing = Saml2X509Credential.signing(privateKey, relyingPartyCertificate());
        Saml2X509Credential decryption = Saml2X509Credential.decryption(privateKey, relyingPartyCertificate());
        Saml2X509Credential encryption = Saml2X509Credential.encryption(assertingPartyCertificate());
        Saml2X509Credential verification = Saml2X509Credential.verification(assertingPartyCertificate());
        RelyingPartyRegistration ddr = RelyingPartyRegistrations
                .fromMetadataLocation(IdPMetadataURL)
                .assertingPartyDetails(party -> party
                        .verificationX509Credentials(c -> c.add(verification))
                        .encryptionX509Credentials(c -> c.add(encryption))
                        .wantAuthnRequestsSigned(true)
                )
                .registrationId("s3point").singleLogoutServiceLocation(baseUrl + "/logout/saml2/slo")
                .signingX509Credentials((c) -> c.add(signing))
                .decryptionX509Credentials(c -> c.add(decryption))
                .build();
        return new InMemoryRelyingPartyRegistrationRepository(ddr);
    }

    private X509Certificate relyingPartyCertificate() {
        Resource resource = new ClassPathResource(certificate);
        try (InputStream is = resource.getInputStream()) {
            return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(is);
        } catch (Exception ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

    private X509Certificate assertingPartyCertificate() {
        Resource resource = new ClassPathResource(IdPCertificate);
        try (InputStream is = resource.getInputStream()) {
            return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(is);
        } catch (Exception ex) {
            throw new UnsupportedOperationException(ex);
        }
    }

    private Extensions extensions() {
        ExtensionsBuilder builder = new ExtensionsBuilder();
        Extensions extensions = builder.buildObject();

        UIInfoBuilder uiInfoBuilder = new UIInfoBuilder();
        UIInfo uiInfo = uiInfoBuilder.buildObject();

        DisplayNameBuilder displayNameBuilder = new DisplayNameBuilder();
        DisplayName displayNameEN = displayNameBuilder.buildObject();
        displayNameEN.setValue("Data Driven Research");
        displayNameEN.setXMLLang("en");

        DisplayNameBuilder displayNameBuilderCZ = new DisplayNameBuilder();
        DisplayName displayNameCZ = displayNameBuilderCZ.buildObject();
        displayNameCZ.setValue("Data Driven Research");
        displayNameCZ.setXMLLang("cs");

        DescriptionBuilder descriptionBuilder = new DescriptionBuilder();
        Description descriptionEN = descriptionBuilder.buildObject();
        descriptionEN.setValue("Application Data Driven Research");
        descriptionEN.setXMLLang("en");

        DescriptionBuilder descriptionBuilderCZ = new DescriptionBuilder();
        Description descriptionCZ = descriptionBuilderCZ.buildObject();
        descriptionCZ.setValue("Aplikace Data Driven Research");
        descriptionCZ.setXMLLang("cs");

        InformationURLBuilder informationURLBuilder = new InformationURLBuilder();
        InformationURL informationURLEN = informationURLBuilder.buildObject();
        informationURLEN.setURI(baseUrl);
        informationURLEN.setXMLLang("en");

        InformationURLBuilder informationURLBuilderCZ = new InformationURLBuilder();
        InformationURL informationURLCZ = informationURLBuilderCZ.buildObject();
        informationURLCZ.setURI(baseUrl);
        informationURLCZ.setXMLLang("cs");

        uiInfo.getDisplayNames().add(displayNameEN);
        uiInfo.getDisplayNames().add(displayNameCZ);
        uiInfo.getDescriptions().add(descriptionEN);
        uiInfo.getDescriptions().add(descriptionCZ);
        uiInfo.getInformationURLs().add(informationURLEN);
        uiInfo.getInformationURLs().add(informationURLCZ);

        RequestInitiatorBuilder requestInitiatorBuilder = new RequestInitiatorBuilder();
        RequestInitiator requestInitiator = requestInitiatorBuilder.buildObject();
        requestInitiator.setBinding(RequestInitiator.REQUIRED_BINDING_VALUE);
        requestInitiator.setLocation(baseUrl + "/saml2/authenticate/s3point");

        extensions.getUnknownXMLObjects().add(uiInfo);
        extensions.getUnknownXMLObjects().add(requestInitiator);

        return extensions;
    }

    private Organization organization() {
        OrganizationBuilder organizationBuilder = new OrganizationBuilder();
        Organization organization = organizationBuilder.buildObject();

        OrganizationNameBuilder organizationNameBuilder = new OrganizationNameBuilder();
        OrganizationName organizationNameEN = organizationNameBuilder.buildObject();
        organizationNameEN.setValue(organizationNameENVal);
        organizationNameEN.setXMLLang("en");

        OrganizationNameBuilder organizationNameBuilderCZ = new OrganizationNameBuilder();
        OrganizationName organizationNameCZ = organizationNameBuilderCZ.buildObject();
        organizationNameCZ.setValue(organizationNameCZVal);
        organizationNameCZ.setXMLLang("cs");

        OrganizationDisplayNameBuilder displayNameBuilder = new OrganizationDisplayNameBuilder();
        OrganizationDisplayName organizationDisplayNameEN = displayNameBuilder
                .buildObject();
        organizationDisplayNameEN.setValue(organizationNameENVal);
        organizationDisplayNameEN.setXMLLang("en");

        OrganizationDisplayNameBuilder displayNameBuilderCZ = new OrganizationDisplayNameBuilder();
        OrganizationDisplayName organizationDisplayNameCZ = displayNameBuilderCZ.buildObject();
        organizationDisplayNameCZ.setValue(organizationNameCZVal);
        organizationDisplayNameCZ.setXMLLang("cs");

        OrganizationURLBuilder organizationURLBuilder = new OrganizationURLBuilder();
        OrganizationURL organizationURL = organizationURLBuilder.buildObject();
        organizationURL.setURI(organizationURLENVal);
        organizationURL.setXMLLang("en");

        OrganizationURLBuilder organizationURLBuilderCZ = new OrganizationURLBuilder();
        OrganizationURL organizationURLCZ = organizationURLBuilderCZ.buildObject();
        organizationURLCZ.setURI(organizationURLCZVal);
        organizationURLCZ.setXMLLang("cs");

        organization.getOrganizationNames().add(organizationNameEN);
        organization.getOrganizationNames().add(organizationNameCZ);
        organization.getDisplayNames().add(organizationDisplayNameEN);
        organization.getDisplayNames().add(organizationDisplayNameCZ);
        organization.getURLs().add(organizationURL);
        organization.getURLs().add(organizationURLCZ);

        return organization;
    }

    private ContactPerson contactPerson() {
        ContactPersonBuilder builder = new ContactPersonBuilder();
        ContactPerson contactPerson = builder.buildObject();

        GivenNameBuilder givenNameBuilder = new GivenNameBuilder();
        GivenName givenName = givenNameBuilder.buildObject();
        givenName.setValue(contactPersonGivenName);

        SurNameBuilder surNameBuilder = new SurNameBuilder();
        SurName surName = surNameBuilder.buildObject();
        surName.setValue(contactPersonSurname);

        EmailAddressBuilder emailAddressBuilder = new EmailAddressBuilder();
        EmailAddress emailAddress = emailAddressBuilder.buildObject();
        emailAddress.setURI("mailto:" + contactPersonEmail);

        contactPerson.setGivenName(givenName);
        contactPerson.setSurName(surName);
        contactPerson.setType(ContactPersonTypeEnumeration.TECHNICAL);
        contactPerson.getEmailAddresses().add(emailAddress);

        return contactPerson;
    }

    private Conditions conditions() {
        ConditionsBuilder conditionsBuilder = new ConditionsBuilder();
        Conditions conditions = conditionsBuilder.buildObject();

        AudienceRestrictionBuilder builder = new AudienceRestrictionBuilder();
        AudienceRestriction audienceRestriction = builder.buildObject();

        AudienceBuilder audienceBuilder = new AudienceBuilder();
        Audience audience = audienceBuilder.buildObject();
        audience.setURI(baseUrl + "/saml2/service-provider-metadata/s3point");
        audienceRestriction.getAudiences().add(audience);

        conditions.getAudienceRestrictions().add(audienceRestriction);
        return conditions;
    }
}
