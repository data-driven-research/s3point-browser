package cz.tul.cxi.s3point.config;

import cz.tul.cxi.s3point.model.jpa.User;
import cz.tul.cxi.s3point.model.auxiliary.DepartmentAcronym;
import cz.tul.cxi.s3point.model.auxiliary.UserRole;
import cz.tul.cxi.s3point.repository.jpa.UserRepository;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.schema.*;
import org.opensaml.saml.saml2.core.*;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.saml2.Saml2LoginConfigurer;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.saml2.provider.service.authentication.DefaultSaml2AuthenticatedPrincipal;
import org.springframework.security.saml2.provider.service.authentication.OpenSaml4AuthenticationProvider;
import org.springframework.security.saml2.provider.service.authentication.Saml2Authentication;
import org.springframework.security.saml2.provider.service.authentication.Saml2AuthenticationToken;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.IOException;
import java.util.*;

/*
Grand thanks to contributors of https://github.com/jim-reespotter/SpringBootSAML
 */
@Component
@Slf4j
class Saml2LoginConfig implements Customizer<Saml2LoginConfigurer<HttpSecurity>>, UserDetailsService {

    private final UserRepository userRepository;

    private UserRole userRole;

    Saml2LoginConfig(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void customize(Saml2LoginConfigurer<HttpSecurity> configurer) {
        configurer.successHandler(new SavedRequestAwareAuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest request,
                                                HttpServletResponse response,
                                                Authentication authentication)
                    throws IOException, ServletException {
                registerUser(authentication);
                authentication = assignAuthorities(authentication);
                super.onAuthenticationSuccess(request, response, authentication);
            }
        });
    }

    private void registerUser(Authentication authentication) {
        DefaultSaml2AuthenticatedPrincipal principal =
                (DefaultSaml2AuthenticatedPrincipal) authentication.getPrincipal();
        String emailAttributeId = "urn:oid:0.9.2342.19200300.100.1.3";
        String email = principal.getFirstAttribute(emailAttributeId);
        assert email != null;
        User user = userRepository.findById(email).orElseGet(() ->
                userRepository.save(new User(email, DepartmentAcronym.GUEST, UserRole.GUEST)));
        userRole = user.getRole();

    }

    private Authentication assignAuthorities(Authentication authentication) {
        Saml2Authentication saml2Authentication = new Saml2Authentication(
                (AuthenticatedPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
                ((Saml2Authentication) authentication).getSaml2Response(),
                new ArrayList<>(Collections.singleton(
                        new SimpleGrantedAuthority("ROLE_" + userRole.name())
                )));
        SecurityContextHolder.getContext().setAuthentication(saml2Authentication);

        return saml2Authentication;
    }

    protected AuthenticationProvider createAuthenticationProvider() {
        OpenSaml4AuthenticationProvider authenticationProvider = new OpenSaml4AuthenticationProvider();
        authenticationProvider.setResponseAuthenticationConverter(responseToken ->
                CustomSaml4AuthenticationProvider
                        .createDefaultResponseAuthenticationConverter()
                        .convert(responseToken)
        );
        return authenticationProvider;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findById(username).orElseThrow();
    }


    static class CustomSaml4AuthenticationProvider {
        public CustomSaml4AuthenticationProvider() {
        }

        public static Converter<OpenSaml4AuthenticationProvider.ResponseToken, Saml2Authentication> createDefaultResponseAuthenticationConverter() {
            return (responseToken) -> {
                Response response = responseToken.getResponse();
                Saml2AuthenticationToken token = responseToken.getToken();
                Assertion assertion = CollectionUtils.firstElement(response.getAssertions());
                assert assertion != null;
                log.debug("Attributes: " + assertion.getAttributeStatements().get(0).getAttributes());
                String username = (String) getXmlObjectValue(assertion.getAttributeStatements().get(0).getAttributes().get(5).getAttributeValues().get(0));
                Map<String, List<Object>> attributes = getAssertionAttributes(assertion);
                List<String> sessionIndexes = getSessionIndexes(assertion);
                DefaultSaml2AuthenticatedPrincipal principal = new DefaultSaml2AuthenticatedPrincipal(username, attributes, sessionIndexes);
                String registrationId = responseToken.getToken().getRelyingPartyRegistration().getRegistrationId();
                principal.setRelyingPartyRegistrationId(registrationId);
                return new Saml2Authentication(principal, token.getSaml2Response(), AuthorityUtils.createAuthorityList("ROLE_GUEST"));
            };
        }

        private static LinkedHashMap<String, List<Object>> getAssertionAttributes(Assertion assertion) {
            MultiValueMap<String, Object> attributeMap = new LinkedMultiValueMap<>();

            for (AttributeStatement attributeStatement : assertion.getAttributeStatements()) {

                for (Attribute attribute : attributeStatement.getAttributes()) {
                    List<Object> attributeValues = new ArrayList<>();

                    for (XMLObject xmlObject : attribute.getAttributeValues()) {
                        Object attributeValue = getXmlObjectValue(xmlObject);
                        if (attributeValue != null) {
                            attributeValues.add(attributeValue);
                        }
                    }

                    attributeMap.addAll(attribute.getName(), attributeValues);
                }
            }

            return new LinkedHashMap<>(attributeMap);
        }

        private static List<String> getSessionIndexes(Assertion assertion) {
            List<String> sessionIndexes = new ArrayList<>();

            for (AuthnStatement statement : assertion.getAuthnStatements()) {
                sessionIndexes.add(statement.getSessionIndex());
            }

            return sessionIndexes;
        }

        private static Object getXmlObjectValue(XMLObject xmlObject) {
            if (xmlObject instanceof XSAny) {
                return ((XSAny) xmlObject).getTextContent();
            } else if (xmlObject instanceof XSString) {
                return ((XSString) xmlObject).getValue();
            } else if (xmlObject instanceof XSInteger) {
                return ((XSInteger) xmlObject).getValue();
            } else if (xmlObject instanceof XSURI) {
                return ((XSURI) xmlObject).getURI();
            } else if (xmlObject instanceof XSBoolean) {
                XSBooleanValue xsBooleanValue = ((XSBoolean) xmlObject).getValue();
                return xsBooleanValue != null ? xsBooleanValue.getValue() : null;
            } else {
                return xmlObject instanceof XSDateTime ? ((XSDateTime) xmlObject).getValue() : xmlObject;
            }
        }
    }
}
