package cz.tul.cxi.s3point.controller;

import cz.tul.cxi.s3point.model.jpa.User;
import cz.tul.cxi.s3point.model.s3.S3Object;
import cz.tul.cxi.s3point.service.S3CoreService;
import cz.tul.cxi.s3point.service.UserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static cz.tul.cxi.s3point.repository.jpa.GenericRepository.JT_NONE;

@Controller
@RequestMapping("/modification")
@Slf4j
public class ModificationController {

    private final UserService userService;

    private final S3CoreService s3c;

    public ModificationController(UserService userService,
                                  S3CoreService s3c) {
        this.userService = userService;
        this.s3c = s3c;
    }


    @PostMapping("/s3dataset/addUser")
    public String getAddUserToEntityForm(@Valid @RequestBody S3Object s3dataset,
                                         Model model) {
        model.addAttribute("loadedUsers", s3c.findUsersOfS3Dataset(s3dataset));
        model.addAttribute("s3dataset", s3dataset);
        model.addAttribute("userToAdd", new User());

        return "modification/user-add";
    }

    @PutMapping(value = "/s3dataset/addUser/{userEmail}", consumes = "application/json")
    public String postAddUserToEntity(Authentication authentication,
                                      @Valid @RequestBody S3Object dataset,
                                      @PathVariable String userEmail) {
       s3c.addUserToDataset(dataset, userService.findById(userEmail).orElseThrow());
        return "redirect:/list/users?byEntity=%s&byId=%s&byAcronym=%s&%s=true";
    }

    @PutMapping(value = "/s3dataset/removeUser/{userEmail}", consumes = "application/json")
    @PreAuthorize("hasRole('ADMINISTRATOR') or #userEmail.equals(authentication.name) or hasRole('MANAGER')")
    public ResponseEntity<String> getRemoveUserFromEntity(Authentication authentication,
                                          @Valid @RequestBody S3Object dataset,
                                          @PathVariable String userEmail) {
        if (s3c.userIsBucketManager(dataset, userService.findById(authentication.getName()).orElseThrow()) ||
                userService.isLoggedUserAdmin(authentication) == JT_NONE) {
            return new ResponseEntity<>("You don't have rights.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        s3c.removeUserFromDataset(dataset, userService.findById(userEmail).orElseThrow());

        return new ResponseEntity<>("User removed.", HttpStatus.OK);
    }

    @PostMapping(value = "/user/updateProfile", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String requestProfileChange(@ModelAttribute User userProfile, RedirectAttributes redirectAttributes) {
        userService.findById(userProfile.getEmail()).ifPresent(user -> {
            user.setRole(userProfile.getRole());
            user.setEinfraId(userProfile.getEinfraId());
            user.setTenant(userProfile.getTenant());
            user.setLang(userProfile.getLang());
            userService.save(user);
        });
        redirectAttributes.addFlashAttribute("success", "user.saved.successfully");
        return String.format("redirect:/detail/user/%s", userProfile.getEmail());
    }

    @PostMapping(value = "/user/updateProfileRequest", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String requestProfileChange(Authentication authentication,
                                       @ModelAttribute User userProfile,
                                       RedirectAttributes redirectAttributes) {
        userService.sendProfileChangeRequest(userProfile, userService.findById(authentication.getName()).orElseThrow());
        redirectAttributes.addFlashAttribute("success", "Údaje byly odeslány ke změně.");
        return String.format("redirect:/detail/user/%s", userProfile.getEmail());
    }
}
