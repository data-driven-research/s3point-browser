package cz.tul.cxi.s3point.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cz.tul.cxi.s3point.service.UserService;

@Controller
@RequestMapping("/dialog")
public class DialogController {

    private final UserService userService;

    public DialogController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/dlg-user/{id}")
    public String getUserDetail(@PathVariable String id, Model model) {
        model.addAttribute("userProfile", userService.findById(id).orElseThrow());
        return "dialog/dlg-user";
    }

    @GetMapping(value = "/dlg-zip")
    public String downloadZip(Model model) {
        return "dialog/dlg-zip";
    }

    @GetMapping(value = "/dlg-sharepub")
    public String sharePublic(Model model) {
        return "dialog/dlg-sharepub";
    }
}
