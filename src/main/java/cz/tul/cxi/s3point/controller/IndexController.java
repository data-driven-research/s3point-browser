package cz.tul.cxi.s3point.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class IndexController {
    @GetMapping("/")
    public String getConsole() {
        return "redirect:/list/s3points";
    }

    @GetMapping("/not-implemented")
    public String notImplemented() {
        return "not-implemented";
    }

    @GetMapping("/login")
    String login() {
        return "login";
    }
}