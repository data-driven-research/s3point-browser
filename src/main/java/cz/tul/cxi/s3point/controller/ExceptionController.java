package cz.tul.cxi.s3point.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.thymeleaf.exceptions.TemplateProcessingException;
import software.amazon.awssdk.services.s3.model.S3Exception;

import java.io.IOException;
import java.util.MissingResourceException;

@Controller
@Slf4j
public class ExceptionController implements ErrorController {

    @GetMapping("/error")
    public String handleError(Model model,
                              HttpServletRequest request) {
        model.addAttribute("error",
                String.format(
                        "Code: %s",
                        request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE)
                )
        );
        model.addAttribute("reason",
                "An unhandled exception occurred in the program.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @PostMapping("/error")
    public String handleErrorPost(Model model,
                                  HttpServletRequest request) {
        model.addAttribute("error",
                String.format(
                        "Code: %s",
                        request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE)
                )
        );
        model.addAttribute("reason",
                "An unhandled exception occurred in the program.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = IllegalAccessException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected String handleIllegalAccess(RuntimeException ex,
                                         Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "You do not possess rights to view this resource.");
        model.addAttribute("solution",
                "If you think you should have rights, modify your profile, " +
                        "ask your colleague or contact the administrator.");
        return "error";
    }

    @ExceptionHandler(value = IllegalStateException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleIllegalState(RuntimeException ex,
                                        Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "The application comes to inconsistent state.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String handleIllegalArgument(RuntimeException ex,
                                           Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "You have given the application an illegal argument");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = MailException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleMailEx(RuntimeException ex,
                                  Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "There was an error with in-built mail client.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = S3Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleS3Ex(RuntimeException ex,
                                Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "There was an error with in-built S3 client.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = MissingResourceException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected String handleMissingResource(RuntimeException ex,
                                           Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "The requested resource not found.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = IOException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleIOEx(RuntimeException ex,
                                Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "There was a problem with access to data sources.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = TemplateProcessingException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected String handleThymeleafEx(RuntimeException ex,
                                       Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "There was a problem with rendering UI.");
        model.addAttribute("solution",
                "Try again. If problem persists, contact administrator.");
        return "error";
    }

    @ExceptionHandler(value = HttpClientErrorException.Forbidden.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected String handleForbiddenEx(Exception ex,
                                       Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "You accessed a page or did an action without proper rights to do so.");
        model.addAttribute("solution",
                "If you think you should have rights, modify your profile, " +
                        "ask your colleague or contact the administrator.");
        return "error";
    }

    @ExceptionHandler(value = HttpClientErrorException.NotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected String handleNotFoundEx(Exception ex,
                                      Model model) {
        log.error(ex.getMessage());
        model.addAttribute("error", ex.getMessage());
        model.addAttribute("reason",
                "You misspelled URL or there is wrong link on previous page.");
        model.addAttribute("solution",
                "Check your URL, try again and if problem persist, " +
                        "contact admin with error description.");
        return "error";
    }
}
