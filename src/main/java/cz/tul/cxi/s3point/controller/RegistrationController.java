package cz.tul.cxi.s3point.controller;

import cz.tul.cxi.s3point.model.s3.S3Bucket;
import cz.tul.cxi.s3point.model.s3.S3Object;
import cz.tul.cxi.s3point.service.*;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/registration")
@Slf4j
public class RegistrationController {

    private final UserService userService;

    private final S3CoreService s3c;

    public RegistrationController(UserService userService,
                                  S3CoreService s3c) {
        this.userService = userService;
        this.s3c = s3c;
    }

    @PostMapping(value = "/s3bucket", consumes = "application/json")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ResponseEntity<String> postBucket(Authentication authentication,
                                     @Valid @RequestBody S3Bucket s3Bucket,
                                     Model model) {
        log.debug("User: " + authentication + " creating " + s3Bucket);
        s3c.createBucket(s3Bucket);

        return new ResponseEntity<>("Bucket created", HttpStatus.OK);
    }

    @PostMapping(value = "/s3dataset", consumes = "application/json")
    public ResponseEntity<String> postDataset(Authentication authentication,
                              @Valid @RequestBody S3Object dataset,
                              Model model) {
        log.debug("User: " + authentication + " creating " + dataset);
        s3c.createDataset(dataset, userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>("Dataset created", HttpStatus.OK);
    }


}
