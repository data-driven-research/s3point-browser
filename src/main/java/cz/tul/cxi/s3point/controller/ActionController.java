package cz.tul.cxi.s3point.controller;

import cz.tul.cxi.s3point.model.s3.S3Object;
import cz.tul.cxi.s3point.service.*;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/action")
@Slf4j
public class ActionController {

    private final UserService userService;

    private final S3CoreService s3CoreService;

    public ActionController(UserService userService,
                            S3CoreService s3CoreService) {
        this.userService = userService;
        this.s3CoreService = s3CoreService;
    }

    @PostMapping(value = "/s3dataset/download", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> downloadS3Dataset(Authentication authentication,
                                                    @Valid @RequestBody S3Object s3Dataset) {
        log.debug("User: " + authentication + " downloading " + s3Dataset);

        String urls = s3CoreService.downloadS3Dataset(s3Dataset,
                userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>(urls, HttpStatus.OK);
    }

    @PostMapping(value = "/s3dataset/publicURL", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> publicShare(Authentication authentication,
                                                    @Valid @RequestBody S3Object s3Dataset) {
        log.debug("User: " + authentication + " sharing " + s3Dataset);

        String urls = s3CoreService.getPublicURLs(s3Dataset,
                userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>(urls, HttpStatus.OK);
    }

    @PostMapping(value = "/s3object/download", consumes = "application/json", produces = "application/json")
    public ResponseEntity<String> downloadFile(Authentication authentication,
                                               @Valid @RequestBody S3Object s3Object) {
        log.debug("User: " + authentication + " downloading " + s3Object);

        String result = s3CoreService.downloadFile(s3Object,
                userService.findById(authentication.getName()).orElseThrow());

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
