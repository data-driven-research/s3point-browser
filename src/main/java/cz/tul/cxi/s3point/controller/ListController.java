package cz.tul.cxi.s3point.controller;

import cz.tul.cxi.s3point.service.UserService;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/list")
public class ListController {

    private final UserService userService;

    public ListController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/s3points")
    public String getS3points() {
        return "list/s3points";
    }

    @GetMapping("/users")
    @PreAuthorize("!#byEntity.equals('admin') or hasRole('ADMINISTRATOR')")
    public String getUsersList(Model model) {
        model.addAttribute("loadedUsers", userService.findAll());

        return "list/users";
    }

    @GetMapping("/alerts")
    public String getAlerts() {
        return "list/alerts";
    }

}
