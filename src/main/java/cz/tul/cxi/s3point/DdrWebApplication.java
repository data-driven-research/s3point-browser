package cz.tul.cxi.s3point;

import cz.tul.cxi.s3point.repository.jpa.GenericRepositoryImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(repositoryBaseClass = GenericRepositoryImpl.class,
        basePackages = "cz.tul.cxi.s3point.repository.jpa")
public class DdrWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DdrWebApplication.class, args);
    }

}
