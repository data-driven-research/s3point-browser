package cz.tul.cxi.s3point.model.jpa;

import cz.tul.cxi.s3point.model.auxiliary.DepartmentAcronym;
import cz.tul.cxi.s3point.model.auxiliary.UserRole;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.*;

@Entity
@Table(name = "users")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class User implements Comparable<User>, UserDetails {
    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "tenant")
    private Long tenant = 1L;

    @Enumerated(EnumType.STRING)
    @Column(name = "department")
    private DepartmentAcronym uDepartmentAcronym = DepartmentAcronym.GUEST;

    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private UserRole role = UserRole.GUEST;

    @Column(name = "e_infra_id")
    private String einfraId;

    @Column(name = "lang", length = 2)
    private String lang = "cs";

    @Column(name = "card_id")
    private String cardID;

    public User(String email, DepartmentAcronym uDepartmentAcronym, UserRole role) {
        this.email = email;
        this.uDepartmentAcronym = uDepartmentAcronym;
        this.role = role;
    }

    public User(String cardID) {
        this.cardID = cardID;
    }

    @Override
    public int compareTo(User o) {
        return this.email.toLowerCase(Locale.forLanguageTag("cs")).compareTo(o.email.toLowerCase(Locale.forLanguageTag("cs")));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority("ROLE_" + this.role.name()));
    }

    @Override
    public String getPassword() {
        return this.cardID;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
