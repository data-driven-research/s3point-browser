package cz.tul.cxi.s3point.model.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;

@Entity
@Table(name = "alerts")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Alert implements Comparable<Alert> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "alert_type")
    private Long alertType;

    @Column(name = "alert_message")
    private String alertMessage;

    @Column(name = "alert_text")
    private String alertText;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_email", nullable = false)
    private User user;

    @Column(name = "create_ts", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp createTs;

    @Column(name = "expire_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Timestamp expireTs;

    @Override
    public int compareTo(Alert o) {
        return this.id.compareTo(o.id);
    }
}
