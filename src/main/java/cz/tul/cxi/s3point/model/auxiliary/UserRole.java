package cz.tul.cxi.s3point.model.auxiliary;

public enum UserRole {
    ADMINISTRATOR("Administrator", "Administrátor"),
    MANAGER("Instrument manager", "Správce přístroje"),
    OPERATOR("Instrument operator", "Operátor přístroje"),
    RESEARCHER("Researcher", "Výzkumník"),
    GUEST("Guest", "Host");

    private final String en;

    private final String cs;

    UserRole(String en, String cs) {
        this.en = en;
        this.cs = cs;
    }

    public String en() {
        return en;
    }

    public String cs() {
        return cs;
    }
}
