package cz.tul.cxi.s3point.utility;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

public class ListOfStringsToStringConverter
        implements Converter<List<String>, String> {

    @Override
    public String convert(@NonNull List<String> from) {
        return StringUtils.join(from, ", ");
    }
}
