package cz.tul.cxi.s3point.component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.tul.cxi.s3point.model.s3.S3Bucket;
import cz.tul.cxi.s3point.model.s3.S3Object;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class S3CoreComponent {

    private final RestTemplate restTemplate;

    private final ObjectMapper mapper;

    @Value("${s3.vo_name}")
    private String voName;

    @Value("${s3.admin_name}")
    private String adminName;

    @Value("${s3core.url}")
    private String s3CoreUrl;

    @Value("${s3core.api_key}")
    private String apiKey;

    @Value("${s3core.api_secret}")
    private String apiSecret;


    public S3CoreComponent(RestTemplateBuilder builder) {
        this.restTemplate = builder.build();
        this.mapper = new ObjectMapper();
    }

    private HttpHeaders getDefaultHeaders(String einfraId) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-API-KEY", apiKey);
        headers.add("X-API-SECRET", apiSecret);
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "application/json");
        headers.add("Accept-Charset", "utf-8");
        headers.add("Accept-Encoding", "gzip, deflate");
        headers.add("tenantId", voName);
        headers.add("einfraId", einfraId);
        return headers;
    }

    public String listObjects(S3Object s3Dataset, String einfraId) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Dataset), getDefaultHeaders(einfraId));
        return restTemplate.exchange(s3CoreUrl + "/objects/list", HttpMethod.POST, entity, String.class).getBody();

    }

    public String listDatasets(S3Bucket s3Bucket, String einfraId) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Bucket), getDefaultHeaders(einfraId));
        return restTemplate.exchange(s3CoreUrl + "/datasets/list", HttpMethod.POST, entity, String.class).getBody();
    }

    public String listBuckets() {
        HttpEntity<String> entity = new HttpEntity<>(null, getDefaultHeaders(adminName));
        return restTemplate.exchange(s3CoreUrl + "/buckets/list", HttpMethod.GET, entity, String.class).getBody();
    }

    public String getPreSignedULRs(S3Object s3Dataset, String einfraId) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Dataset), getDefaultHeaders(einfraId));
        return restTemplate.exchange(s3CoreUrl + "/objects/downloadUrls", HttpMethod.POST, entity, String.class).getBody();
    }

    public String downloadFile(S3Object s3Object, String einfraId) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Object), getDefaultHeaders(einfraId));
        return restTemplate.exchange(s3CoreUrl + "/objects/download", HttpMethod.POST, entity, String.class).getBody();
    }

    public void shareDataset(S3Object s3Object, String einfraId) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Object), getDefaultHeaders(adminName));
        restTemplate.exchange(s3CoreUrl + "/datasets/share?einfraId="+einfraId, HttpMethod.POST, entity, String.class).getBody();
    }

    public void createBucket(S3Bucket s3Bucket) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Bucket), getDefaultHeaders(adminName));
        restTemplate.exchange(s3CoreUrl + "/buckets/create", HttpMethod.PUT, entity, String.class);
    }

    public void createDataset(S3Object s3Object, String einfraId) throws JsonProcessingException {
        HttpEntity<String> entity = new HttpEntity<>(mapper.writeValueAsString(s3Object), getDefaultHeaders(adminName));
        restTemplate.exchange(s3CoreUrl + "/datasets/create?einfraId="+einfraId, HttpMethod.PUT, entity, String.class);
    }

    public void removeUserFromDataset(S3Object s3dataset, String einfraId) {
        restTemplate.put(s3CoreUrl + "/datasets/unshare?einfraId=" + einfraId, s3dataset);
    }

    public boolean userIsBucketManager(S3Bucket s3Bucket, String einfraId) {
        return Boolean.TRUE.equals(restTemplate.postForObject(s3CoreUrl + "/buckets/isManager?einfraId=" + einfraId, s3Bucket, Boolean.class));
    }

    public String findUsersOfS3Dataset(S3Object s3dataset) {
        return restTemplate.postForObject(s3CoreUrl + "/datasets/users", s3dataset, String.class);
    }
}
