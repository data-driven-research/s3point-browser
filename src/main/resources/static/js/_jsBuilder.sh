#!/bin/bash

# Delete old file if it exists
echo "------- deleting old file -------"
rm -f _s3point.js

# Concatenate JavaScript files
echo "----- compiling javascripts -----"
cat functions.js jquery.init.js DGrid.js DControl.js DGridAlert.js DGridS3Point.js >> _s3point.js
