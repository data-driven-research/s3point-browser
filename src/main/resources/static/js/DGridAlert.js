class DGridAlert extends DGrid {

  constructor (res, gridId, gridName, endpoint) {
    super(res, gridId, gridName, endpoint); 

    this.initGrid();
  }

  initEvents() {
    const helper=this;
    
    super.initEvents();
    
    $('#filter2Btn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');
      
      let filter2={mode: 'AND', data:[]};

      let text=$('#text').val();
      if (text)          
        filter2.data.push({dataIndx: 'alertText', value: text, condition: 'contain'});

      let info=$('#info');
      if (!info.prop('checked')) {
        filter2.data.push({dataIndx: 'alertType', value: '1', condition: 'equal'});
      }
      
      if (filter2.data.length>0) {
        grid.options.filterModel.mode='AND';  
        grid.options.dataModel.postData={filter2: JSON.stringify(filter2)};
      }
      else {
        grid.options.filterModel.mode='OR';  
        grid.options.dataModel.postData=null;
      }
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();      
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });

    $('#filter2ClearBtn').on('click', function(evt){
      const grid=$(helper.gridId).pqGrid('instance');

      $('#text').val('');

      grid.options.filterModel.mode='OR';  
      grid.options.dataModel.postData=null;
        
      controls.spinBtn('fa-filter', '#filter2Btn');
      grid.refreshDataAndView();     
      controls.unspinBtn('fa-filter', '#filter2Btn');
    });
    
    $(document).on('click', '#gdelete', function(evt){
      const id=$(this).data('id');
      console.log('delete alert '+id)
    });
    
  }
  
  initGrid() {
    const helper=this;

    let colM1 = [
      { title: '', width: 50, dataIndx: 'alertType', align: 'center', resizable: false, copy: false, render: this.pqRenderAlert,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.message, width: 250, dataIndx: 'alertMessage', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.text, width: 450, dataIndx: 'alertText', resizable: false, render: this.pqRenderFiltered,
        filter: { type: 'textbox', condition: 'contain', listeners: ['keyup'] }
      },
      { title: res.created, width: 160, dataIndx: 'createTs', align: 'right', resizable: false, render: this.pqRenderDateTime,
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: res.expired, width: 160, dataIndx: 'expireTs', align: 'right', resizable: false, render: this.pqRenderDateTime, 
        filter: {type: 'checkbox', subtype: 'triple', condition: 'equal', listeners: ['click'] }      
      },
      { title: '', width: 60, resizable: false, sortable: false, copy: false,
        render: function(ui) {
          let html='';
          
          html+='<div class="grid-icons" style="padding-left: 8px; display: none;">';
          html+='  <button id="gdelete" data-id="'+ui.rowData.id+'" class="btn btn-outline-secondary btn-grid mr-2" type="button"><span class="fa-solid fa-trash" title="smazat"></span></button>';
          html+='</div>';

          return {
            text: html
          }
        } 
      }
    ];
    
    let dataM1 = {
      location: 'remote',
      dataType: 'JSON',
      method: 'POST',
      url: helper.endpoint,
      getData: function (data) {
        console.log(data);
        return { curPage: data.curPage, totalRecords: data.totalRecords, data: data.data };
      }
    }
    
    let grid1 = {
      width: 'auto',
      height: '430',
      sortModel: {
        type: 'remote',
        single: false,
        sorter: [{ dataIndx: 'createdTs', dir: 'down' }],
        space: true,
        multiKey: null
      },
      filterModel: {
        on: true,
        type: 'remote',
        mode: 'AND',
        header: false
      },
      colModel: colM1,
      dataModel: dataM1,
      pageModel: {
        type: 'remote',
        rPP: 20, 
        rPPOptions:[1, 20, 50, 100, 500],
        strRpp: "{0}", 
        strDisplay: "{0} - {1} / {2}"
      },
      selectionModel: {
        type: 'row',
        mode: 'single',
        toggle: false,
        fireSelectChange: true
      },
      scrollModel: {
        theme: true,
        autoFit: true
      },
      autofil: false,
      bootstrap: true,
      collapsible: false,
      draggable: false,
      dragColumns: { enabled: false },
      wrap: false, 
      fillHandle: '',
      hwrap: false,
      hoverMode: 'row',
      hoverRow: -1,
      //freezeCols: 2,            
      numberCell: false,
      resizable: false,
      roundCorners: false,
      rowHt: 40,
      showTitle: false,
      showTop: true,
      showBottom: true,
      virtualX: true,
      virtualY: true,
      
      create:function( event, ui ) {        
        const pager=$(helper.gridId).find('.pq-grid-footer');
        let html = '<span id="gexcel" class="pq-ui-button ui-widget-header" style="cursor: pointer; background: #efdfff; border: 1px solid #af9fff;" tabindex="0" rel="tooltip" title="Excel"><span class="ui-icon ui-icon-disk"></span></span>';
        pager.prepend('<span class="pq-separator"></span>');
        pager.prepend(html);
      },
       
      cellClick: function (evt, ui) {
        $(this).parent().find('.grid-icons').show();
      }
      
    }  
    $(helper.gridId).pqGrid(grid1);
  }
}
