const MSG_WAIT                   = 0;
const MSG_OK                     = 1;
const MSG_WARNING                = 2;
const MSG_ERROR                  = 3;

class DControl {

  constructor () {
    this.initEvents();
  }

  initEvents() {
    const helper=this;

    //tristate onlick rolls state behavior including indeterminated
    $(document).on('click', '.tri-state', function(evt) {
      const control=$(this);
      
      if (control.data('state')=='none')
        control.data('state', 'checked').prop('checked', true).prop('indeterminate', false);
      else if (control.data('state')=='checked')    
        control.data('state', 'unchecked').prop('checked', false);
      else if (control.data('state')=='unchecked')    
        control.data('state', 'none').prop('indeterminate', true);
        
      evt.stopPropagation();  
    })
  }  
  
  setState(selector, state) {
    const helper=this;
    const control=$(selector);
    
    //in order to tristate works properly, we must set indeterminate programatically
    control.data('state', state);
    if (state=='none')
      control.prop('indeterminate', 'true');
  }
  
  dlgOpen(dlgname, path, params, callback) {
    const helper=this;

    $('.toasters').after('<div class="modal modal-right fade" id="' + dlgname + '" tabindex="-1" role="dialog" aria-hidden="true"></div>');
    $('#' + dlgname).load(cons.ctx + 'dialog/' + dlgname + path + '?jsoncallback=' + uuid(), function() {
  
      //call function to setup custom parameters
      setDlgParams(params, callback);
      
      //open modal
      $('#' + dlgname).modal('show');
    });
  }
  
  dlgClose(dlgname) {
    $('#' + dlgname).modal('hide');
    setTimeout(function () {
      $('#' + dlgname).remove();
    }, 500);
  }
  
  loaderShow(value) {
    $('.progress').show();
    loaderAdd(value);
  }
  
  loaderAdd(value) {
    var val=value;
    if (!value) {
      val=4;
    }
    var bar=$('.progress-bar');
    var width=parseInt(bar.attr('aria-valuenow'))+val;
    if (width>100)
      width=0;
    bar.attr('aria-valuenow', width);
    bar.width(width+'%');
    if (!value) {
      setTimeout(addLoader, 1000);
    }
  }
  
  loaderHide() {
    $('.progress').hide();
  }
  
  messageShow(type, timeout, msg) {
    var idx=$('.toasters').children().length+1;
    var cls='';
    var clr='';
    var txt='';
    switch (type) {
      case MSG_WAIT: cls='fa-spinner fa-spin'; break;
      case MSG_OK: cls='fa-check'; clr='lightgreen'; txt='Info'; break;
      case MSG_WARNING: cls='fa-exclamation-triangle'; clr='orange'; txt='Warning'; break;
      case MSG_ERROR: cls='fa-exclamation-triangle'; clr='red'; txt='Error'; timeout=9000; break;
    }
  
    var ahtml='';
    ahtml+='<div id="toast'+idx+'" class="toast" role="alert" aria-live="assertive" aria-atomic="true">';
    ahtml+='  <div class="toast-header">';
    ahtml+='    <span class="fa-solid '+cls+' mr-2" style="font-size: 20px; color: '+clr+';"></span>';
    ahtml+='    <strong class="mr-auto mt-1">'+txt+'</strong>';
    ahtml+='    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">';
    ahtml+='      <span aria-hidden="true" class="fa fa-xmark mt-1" style="font-size: 20px;"></span>';
    ahtml+='    </button>';
    ahtml+='  </div>';
    ahtml+='  <div class="toast-body text-wrap">';
    ahtml+='     <small>'+msg+'</small>';
    ahtml+='  </div>';
    ahtml+='</div>';
  
    $('.toasters').append(ahtml);
    
    var options={};
    if (timeout) {
      options.autohide=true;
      options.delay=timeout;
    }
    else {
      options.autohide=false;
    }
    $('#toast'+idx).toast(options);
    $('#toast'+idx).toast('show');
    
  }

  spinBtn(cls, btn) {
    let sel=(btn) ? btn+' .'+cls : '.'+cls;
    $(sel).removeClass(cls).addClass('fa-spinner fa-spin');
  }
  
  unspinBtn(cls, btn) {
    let sel=(btn) ? btn+' .fa-spin' : '.fa-spin';
    $(sel).removeClass('fa-spinner fa-spin').addClass(cls);
  }

}