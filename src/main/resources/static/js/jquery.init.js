$(document).ready(function() {
	
	if ($.datepicker)	{	
		$.datepicker.regional['en'] = {
			closeText : 'Close',
			prevText : 'Previous',
			nextText : 'Next',
			currentText : 'Now',
			monthNames : [
				'january', 'february', 'march', 'april', 'may', 'june', 'july',	'august', 'september', 'october', 'november', 'december'
			],
			monthNamesShort : [
				'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'
			],
			dayNames : [
				'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'
			],
			dayNamesShort : [
				'su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'
			],
			dayNamesMin : [
				'su', 'mo', 'tu', 'we', 'th', 'fr', 'sa'
			],
			weekHeader : 'KW',
			dateFormat : "dd.MM.yyyy".toLowerCase().replaceAll('yyyy', 'yy'), 
			firstDay : 0,
			isRTL : false,
			showMonthAfterYear : false,
			yearSuffix : '',
			numberOfMonths : 1,
			changeMonth: true,
			changeYear: true,
	    yearRange: "-20Y:+1Y",
	  	beforeShowDay: weekendOrHoliday,
		};
		$.datepicker.regional['cs'] = {
			closeText : 'Zavřít',
			prevText : 'Předchozí',
			nextText : 'Následující',
			currentText : 'Nyní',
			monthNames : [
				'leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec',	'srpen', 'září', 'říjen', 'listopad', 'prosinec'
			],
			monthNamesShort : [
				'led', 'úno', 'bře', 'dub', 'kvě', 'čer', 'čvc', 'srp', 'zář', 'říj', 'lis', 'pro'
			],
			dayNames : [
				'neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'
			],
			dayNamesShort : [
				'ne', 'po', 'út', 'st', 'čt', 'pá', 'so'
			],
			dayNamesMin : [
				'ne', 'po', 'út', 'st', 'čt', 'pá', 'so'
			],
			weekHeader : 'KT',
			dateFormat : "yyyy-MM-dd".toLowerCase().replaceAll('yyyy', 'yy'),
			firstDay : 1,
			isRTL : false,
			showMonthAfterYear : false,
			yearSuffix : '',
			numberOfMonths : 1,
			changeMonth: true,
			changeYear: true,
	    yearRange: "-20Y:+1Y",
	  	beforeShowDay: weekendOrHoliday,
		};
		$.datepicker.setDefaults($.datepicker.regional['cs']);
	}
	
	if ($.timepicker)	{	
		$.timepicker.regional['cs'] = {
			timeOnlyTitle : 'Zvolte čas',
			timeText : 'Čas',
			hourText : 'Hod',
			minuteText : 'Min',
			secondText : 'Sec',
			millisecText : 'ms',
			currentText : 'Nyní',
			closeText : 'OK',
			ampm : false,
			showSecond : false,
			showMillisec : false,
			timeFormat : 'hh:mm',
			showButtonPanel : false
		};
		$.timepicker.setDefaults($.timepicker.regional['cs']);
	}
	
	if ($.paramquery)	{	
		var pageModel = {
	    type: 'remote',
	    rPP: 20, 
	    rPPOptions:[20, 50, 100, 500],
	    layout: (isMobileDevice()) ? ["prev", "strPage", "next", "|", "strRpp", "|", "strDisplay"] : ["first","prev","|","strPage", "|","next","last","|","strRpp","|","refresh","|","strDisplay"],
		};
		
	  var selectionModel = {
	    type: 'row',
	    mode: 'single',
      toggle: false,
	    fireSelectChange: true
	  };
	
	  var scrollModel = {
      theme: true,
	  	autofit: false
	  };
	  
	  var dragModel = {
    	on: false
    };
    
    var dropModel = {
    	on: false
    };

  	$.paramquery.pqGrid.prototype.options.pageModel=pageModel;
  	$.paramquery.pqGrid.prototype.options.selectionModel=selectionModel;
  	$.paramquery.pqGrid.prototype.options.scrollModel=scrollModel;
  	$.paramquery.pqGrid.prototype.options.dragModel=dragModel;
  	$.paramquery.pqGrid.prototype.options.dropModel=dropModel;

	}
  
});

var holidays = [
	"01/01/2022", "04/15/2022", "04/18/2022",	"05/01/2022", "05/08/2022", "07/05/2022", "07/06/2022", "09/28/2022", "10/28/2022",	"11/17/2022", "12/24/2022", "12/25/2022", "12/26/2022",
	"01/01/2023", "04/07/2023", "04/10/2023",	"05/01/2023", "05/08/2023", "07/05/2023", "07/06/2023", "09/28/2023", "10/28/2023",	"11/17/2023", "12/24/2023", "12/25/2023", "12/26/2023",
	"01/01/2024", "03/28/2024", "04/01/2024",	"05/01/2023", "05/08/2023", "07/05/2023", "07/06/2023", "09/28/2023", "10/28/2023",	"11/17/2023", "12/24/2023", "12/25/2023", "12/26/2023",
];

function weekendOrHoliday(date) {
	// No Weekends
	var show = (date.getDay() != 0) && (date.getDay() != 6);
	for (var i = 0; i < holidays.length; i++) {
		if (new Date(holidays[i]).toString() == date.toString()) {
			show = false;
		}// No Holidays
	}

  return [true, (show) ? '' : 'ui-state-highlight', ''];
}
