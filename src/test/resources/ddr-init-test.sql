CREATE TABLE "users"
(
    "email"      text PRIMARY KEY,
    "tenant"     BIGINT DEFAULT 1,
    "department" text DEFAULT 'GUEST',
    "e_infra_id" text,
    "role"       text DEFAULT 'GUEST'
    "lang"       text DEFAULT 'cs'
    "card_id"    text
);

INSERT INTO "users" ("email", "department", "card_id", "role")
VALUES
    ('test.user@tul.cz', 'OMI', '555', 'ADMINISTRATOR'),
    ('jane.smith@example.com', 'ONCH', '534', 'OPERATOR'),
    ('guest@example.com', 'GUEST', '821', 'GUEST'),
    ('cyril.steger@tul.cz','OMI','12234','ADMINISTRATOR');