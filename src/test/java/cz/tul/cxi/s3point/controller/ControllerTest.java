package cz.tul.cxi.s3point.controller;

import cz.tul.cxi.s3point.AbstractContainerBase;
import cz.tul.cxi.s3point.DdrWebApplication;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = DdrWebApplication.class)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ControllerTest extends AbstractContainerBase {
    @Autowired
    private MockMvc mvc;

    @Test
    @Order(1)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testConsole() {
        System.out.println("test console and detail");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
            mvc.perform(MockMvcRequestBuilders
                            .get("/detail/user/test.user@tul.cz")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isOk());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }

    @Test
    @Order(2)
    @WithMockUser(username = "test.user@tul.cz", password = "555", roles = {"ADMINISTRATOR"})
    public void testError() {
        System.out.println("tests error handling");
        try {
            mvc.perform(MockMvcRequestBuilders
                            .get("/non-existent")
                            .accept(MediaType.TEXT_HTML))
                    .andExpect(status().isNotFound());
        } catch (final Exception ex) {
            fail(String.format("%s('%s')\n", ex.getClass().getSimpleName(), ex.getMessage()));
        }
    }
}
