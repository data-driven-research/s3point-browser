package cz.tul.cxi.s3point.service;

import cz.tul.cxi.s3point.AbstractContainerBase;
import cz.tul.cxi.s3point.model.auxiliary.DepartmentAcronym;
import cz.tul.cxi.s3point.model.auxiliary.UserRole;
import cz.tul.cxi.s3point.model.jpa.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ServiceTest extends AbstractContainerBase {
    private static User user;

    @Autowired
    private UserService userService;

    @BeforeAll
    public static void initObjects() {
        user = new User();
        user.setEmail("test.user@tul.cz");
        user.setUDepartmentAcronym(DepartmentAcronym.OMI);
        user.setCardID("555");
        user.setRole(UserRole.ADMINISTRATOR);
    }

    @Test
    @Order(1)
    public void testSaveOfUser() {
        System.out.println("Test save of user");
        userService.save(user);
    }
}
