package cz.tul.cxi.s3point;

import org.testcontainers.containers.PostgreSQLContainer;

public abstract class AbstractContainerBase {
    static final PostgreSQLContainer<?> POSTGRE_SQL_CONTAINER;

    static {
        POSTGRE_SQL_CONTAINER = new PostgreSQLContainer<>(PostgreSQLContainer.IMAGE);
        POSTGRE_SQL_CONTAINER.start();
    }
}
