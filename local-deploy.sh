#!/bin/bash

VERSION=TEST

cd ./src/main/resources/static/js || exit
bash ./_jsBuilder.sh
cd ../../../../../ || exit

./build-release/build.sh $VERSION

cd develop || exit
docker compose up --wait --build