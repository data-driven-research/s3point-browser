CREATE TABLE "users"
(
    "email"      text PRIMARY KEY,
    "tenant"     BIGINT DEFAULT 1,
    "department" text DEFAULT 'GUEST',
    "e_infra_id" text,
    "role"       text DEFAULT 'GUEST'
    "lang"       text DEFAULT 'cs'
    "card_id"    text
);
