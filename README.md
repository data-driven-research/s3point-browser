# S3Point - a CESNET S3 browser

## Description

Web application for browsing and sharing the scientific measurement data and metadata being developed within the CESNET project S3Point implemented at CXI TUL. The backbone of
the web application is the Java framework SpringBoot 3. As a database for the application is used
PostgreSQL. The application is built using Gradle, containerized using Docker, and deployed using
Docker Compose.

[Detailed information about tool in separate wiki](https://gitlab.com/data-driven-research/s3point-core/-/wikis/home).

## Overview
This guide outlines the deployment process for the S3Point web application, developed to manage and share scientific measurement data as part of the CESNET S3Point project, implemented at CXI TUL. The application uses the **SpringBoot 3** Java framework, **PostgreSQL** as its database, and is containerized and managed using **Docker** and **Docker Compose**. 

## Deployment Steps

### 1. Prerequisites
To deploy the S3Point application, ensure you have:
   - Access to a PostgreSQL database.
   - The necessary certificates and keys for Shibboleth Service Provider (SP) metadata and S3 access.
   - Docker and Docker Compose installed on the target server.

### 2. Configurations

#### Docker Compose and Configuration Files
The deployment process is managed through Docker Compose, with configuration files located in the `deploy` directory. Here’s a breakdown of the necessary configuration files and their contents:

1. **`deploy/docker-compose.yaml`**  
   This file contains the main setup for Docker Compose, defining the services for the S3Point web application, the database, and networking. Ensure you replace all placeholder values marked by `<<>>` with actual values.

2. **`deploy/config/application.properties`**  
   Configure application-specific properties here, such as database connection details and any additional Spring Boot configurations.

3. **`deploy/.env`**  
   Environment variables specific to Docker Compose and the deployment environment are set here. This includes sensitive values such as database credentials and API keys.

4. **Certificates and Keys**
   - Store all required certificates and keys in the following directories:
      - **Keystore Directory**: `deploy/keystore`
      - **SSL Directory**: `deploy/ssl`
   - These directories should contain any SSL/TLS certificates for HTTPS access, application certificates, and S3 keys.

### 3. Deployment Process

After configuring the files, follow these steps to deploy the application:

1. **Copy Files**:  
   Copy the entire contents of the `deploy` directory to the server’s target directory.

2. **Run Docker Compose**:
   Navigate to the target directory on the server and run:
   ```bash
   docker compose up --wait

### CESNET S3 Access

For accessing CESNET S3 storage, you need to set up a Virtual Organization (VO). Follow the steps below to configure access:

1. **Set Up a VO**  
   - Contact CESNET support at **support@cesnet.cz**.
   - In your request, specify the required storage size for your organization and provide contact details for at least two designated administrators.
   - Once CESNET approves the VO, access keys tied to your service identity will be issued for accessing S3 data.

2. **Using S3 Access Keys**  
   - These keys can be used for programmatic or GUI-based access to your data on S3.
   - **Note**: This key-based access is a temporary solution, as CESNET is developing a Gatekeeper system that will eventually support federated identity-based access, allowing keys to be generated dynamically based on federation identities.

3. **Further Information**  
   For comprehensive setup instructions, guidelines, and further assistance with CESNET S3, refer to the official DU CESNET guidelines available on their website: [DU CESNET guidelines](https://du.cesnet.cz/cs/navody/start).

---


## Development

To build and deploy as a Docker image locally, you first need to fill *develop/config/application.properties* and
*develop/.env* files. For the Spring Boot profile **test**, there is no need to save and set up SP and IdP files or
any other certificate.

Then for local develoment instance run  
``
./local-deploy.sh
``

***

To build a Java jar you need to run the following:  
``
./build-release/build.sh test
``

Then in the *develop* directory, run:  
``
docker compose up --wait
``

For a production build, you need to run the following:  
``
./build-release/build.sh {target version}
``  
Note: The target versions used by our developer team are
**production** and **stage**.

The container registry is used to release
on [Docker Hub](https://hub.docker.com/r/cxiomi/s3point-browser) with the hub name **cxiomi**, where the
images are uploaded for subsequent deployment. The application is developed so that the actual deployment can be done
with only a change of configuration files in the *deploy* directory. In the case of modifying the code and building an
image for your institution, you need to use your container repository.

The release can be triggered by:  
``
./build-release/release.sh {hub} {target version}
``

The application can be built and released at the same time using the following:  
``
./build-release/build-and-release.sh {hub} {target version}
``



## Authors

Jan Kočí - lead, architect   
Věnceslav Chumchal - developer, architect  
David Vobruba - developer  
Jakub Zach - consultant  

## License

Apache-2.0
